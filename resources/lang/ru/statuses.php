<?php

return [
    'successful_registered' => 'Вы успешно прошли подтверждение регистрации. Теперь можете войти в приложение.',
    'email_already_confirmed' => 'Ваш e-mail уже подтвержден, вы можете войти в приложение.',

];