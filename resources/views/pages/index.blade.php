@extends('pages.layout.layout')

@section('content')
    <section class="flex centered plenum plenum-h first-scr">
        <header class="flex centered">
            @guest
                <div>
                    <a href="{{ route('register') }}" class="btn btn-login">Регистрация</a>
                </div>
            @endguest
            @auth
                <div>
                    <a href="{{ route('account.index') }}" class="btn btn-login">Личный кабинет</a>
                </div>
                <div style="margin-left: 10px">
                    @include('features.buttons.logout', ['class' => 'btn btn-login'])
                </div>
            @endauth
        </header>
        <div class="flex-dimidia card-flex">
            <div class="card">
                <div class="card-image tac">
                    <img src="/images/main-card.png">
                </div>
                <div class="card-additional flex jc-bw">
                    <div>
                        <svg class="ib vam additional-img" width=25 height=30 xmlns="http://www.w3.org/2000/svg"
                             xmlns:xlink="http://www.w3.org/1999/xlink">
                            <image xlink:href="/images/worldwide.svg" width="25" height="30"/>
                        </svg>
                        <div class="ib vam additional-text">По всей стране</div>
                    </div>
                    <div>
                        <div class="ib vam additional-img">
                            <svg width=22 height=30 xmlns="http://www.w3.org/2000/svg"
                                 xmlns:xlink="http://www.w3.org/1999/xlink">
                                <image xlink:href="/images/wireless.svg" width="22" height="30"/>
                            </svg>
                        </div>
                        <div class="ib vam additional-text">Бесконтактная</div>
                    </div>
                    <div>
                        <div class="ib vam additional-img">
                            <svg width=30 height=30 xmlns="http://www.w3.org/2000/svg"
                                 xmlns:xlink="http://www.w3.org/1999/xlink">
                                <image xlink:href="/images/bonuses.svg" width="30" height="30"/>
                            </svg>
                        </div>
                        <div class="ib vam additional-text">Бонусы</div>
                    </div>
                </div>
            </div>
        </div>
        @guest
            <div class="flex-dimidia login-flex">
                <div class="login-form-container">
                    <form method="POST" action="{{ route('login') }}">
                        <div class="login-form">
                            @csrf
                            <span class="lf-small">Войти в учетную запись</span><br>
                            <span class="lf-big">Comfort Card</span>
                            @include('features.fields.text', [
                                'name' => 'login',
                                'attributes' => [
                                    'required',
                                    'autofocus',
                                ],
                                'placeholder' => 'Логин',
                            ])

                            @include('features.fields.text', [
                                'name' => 'password',
                                'type' => 'password',
                                'attributes' => [
                                    'required',
                                ],
                                'placeholder' => 'Пароль',
                            ])
                            <input type="submit" value="Войти">
                            <span>Ваши данные будут использованы исключительно для регистрации и не будут передаваться третьим лицам</span>
                            <!--<div class="steps">-->
                            <!--<div class="step active"></div>-->
                            <!--<div class="step"></div>-->
                            <!--</div>-->
                        </div>
                        <!--<div class="login-form-bg">-->
                        <!--<img src="/images/login-form.png">-->
                        <!--</div>-->
                    </form>
                </div>
            </div>
        @endguest
        <div class="main-bg">
            <div class="figure-1"></div>
            <div class="figure-2"></div>
            <div class="figure-3"></div>
        </div>
    </section>
    <main-page></main-page>
@endsection
