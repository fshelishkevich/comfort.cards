<aside class="menu">
    <p class="menu-label">
        Панель
    </p>
    <ul class="menu-list">
        <li>
            <router-link to="/" exact>Панель</router-link>
        </li>
        <li>
            <router-link to="/orders" exact>Заказы</router-link>
        </li>
    </ul>

    <p class="menu-label">
        Редактирование
    </p>
    <ul class="menu-list">
        <li>
            <router-link to="/users">Пользователи</router-link>
        </li>
        <li>
            <router-link to="/account-types">Типы аккаунтов</router-link>
        </li>
        <li>
            <router-link to="/news">Новости</router-link>
        </li>
        <li>
            <router-link to="/transaction-types">Типы транзакций</router-link>
        </li>
    </ul>

    <p class="menu-label">
        Данные
    </p>
    <ul class="menu-list">
        <li>
            <router-link to="/transactions">Транзакции</router-link>
        </li>
    </ul>

    <p class="menu-label">
        Пользователь
    </p>
    <ul class="menu-list">
        <li>
            <a class="dropdown-item"
               href="{{ route('logout') }}"
               onclick="event.preventDefault();
               document.getElementById('logout-form').submit();"
            >
                Выйти
            </a>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
        </li>
    </ul>
</aside>