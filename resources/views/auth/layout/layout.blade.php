<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/account.css') }}" rel="stylesheet">
</head>
<body>

<header class="account">
    <div class="top-table tac">

        <a href="{{ route('pages.index') }}" class="logo-img"></a>
        <a href="{{ route('pages.index') }}" class="logo-title">Comfort Card</a>

    </div>
</header>

<div class="account-content">
    <div class="content-container">
        <div class="content">
            @yield('form')
        </div>
    </div>
</div>

<!-- Scripts -->
<script src="{{ asset('js/site.js') }}" defer></script>
</body>
</html>
