@extends('auth.layout.layout')

@section('form')
    @include('features.session.show-warning')
    <div class="logreg-form tac">
        <form method="POST" action="{{ route('register') }}">
            @csrf

            <h1>Регистрация</h1>
            @include('features.fields.text', [
                'name' => 'login',
                'attributes' => [
                    'required',
                    'autofocus',
                ],
                'placeholder' => 'Логин (мин.4 символа, латиница)',
            ])

            @include('features.fields.text', [
                'name' => 'email',
                'type' => 'email',
                'attributes' => [
                    'required',
                ],
                'placeholder' => 'E-mail',
            ])

            @include('features.fields.text', [
                'name' => 'password',
                'type' => 'password',
                'attributes' => [
                    'required',
                ],
                'placeholder' => 'Пароль',
            ])

            @include('features.fields.text', [
                'name' => 'password_confirmation',
                'type' => 'password',
                'attributes' => [
                    'required',
                ],
                'placeholder' => 'Повторите пароль',
            ])

            @include('features.fields.text', [
                'name' => 'sponsor_login',
                'value' => $sponsorLogin,
                'label' => 'Логин Вашего спонсора (по рекомендации)',
                'placeholder' => 'Логин спонсора',
            ])

            <button>Регистрация</button>
            <div class="links tac">
                <a href="{{ route('login') }}">Уже есть аккаунт?</a>
            </div>
        </form>
    </div>
@endsection
