@extends('auth.layout.layout')

@section('form')
    @include('features.session.show-status')
    @include('features.session.show-warning')
    <div class="logreg-form tac">
        <form method="POST" action="{{ route('login') }}">
            @csrf

            <h1>Вход</h1>
            @include('features.fields.text', [
                'name' => 'login',
                'attributes' => [
                    'required',
                    'autofocus',
                ],
                'placeholder' => 'Логин',
            ])

            @include('features.fields.text', [
                'name' => 'password',
                'type' => 'password',
                'attributes' => [
                    'required',
                ],
                'placeholder' => 'Пароль',
            ])
            <button>Войти</button>
            <div class="links tac">
                <a href="{{ route('password.request') }}">Забыли пароль?</a> | <a
                        href="{{ route('register') }}">Регистрация</a>
            </div>
        </form>
    </div>
@endsection
