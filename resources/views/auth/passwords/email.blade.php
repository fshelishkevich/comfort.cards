@extends('auth.layout.layout')

@section('form')
    <div class="logreg-form tac">
        @include('features.session.show-status')
        <form method="POST" action="{{ route('password.email') }}">
            @csrf

            @include('features.fields.text', [
                'name' => 'email',
                'type' => 'email',
                'attributes' => [
                    'required',
                ],
                'placeholder' => 'E-mail',
            ])

            <button>Отправить ссылку сброса пароля</button>
        </form>
    </div>
@endsection
