@extends('auth.layout.layout')

@section('form')
    <div class="logreg-form tac">
        @include('features.session.show-status')
        @include('features.session.show-warning')
        <h3>Сброс пароля</h3>
        <form method="POST" action="{{ route('password.request') }}">
            @csrf

            <input type="hidden" name="token" value="{{ $token }}">

            @include('features.fields.text', [
                'name' => 'email',
                'type' => 'email',
                'attributes' => [
                    'required',
                ],
                'placeholder' => 'E-mail',
            ])

            @include('features.fields.text', [
                'name' => 'password',
                'type' => 'password',
                'attributes' => [
                    'required',
                ],
                'placeholder' => 'Пароль',
            ])

            @include('features.fields.text', [
                'name' => 'password_confirmation',
                'type' => 'password',
                'attributes' => [
                    'required',
                ],
                'placeholder' => 'Повторите пароль',
            ])

            <button>Сбросить пароль</button>
        </form>
    </div>
@endsection
