@if(session()->has('status'))
    <div class="cc-status-message">
        {{ session('status') }}
    </div>
@endif