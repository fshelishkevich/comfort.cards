@if(session()->has('warning'))
    <div class="cc-warning-message">
        {{ session('warning') }}
    </div>
@endif