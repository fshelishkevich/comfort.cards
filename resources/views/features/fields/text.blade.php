@if(isset($label))
    <div>{{ $label }}</div>
@endif
@if ($errors->has($name))
    <span class="invalid-feedback">
        <strong>{{ $errors->first($name) }}</strong>
    </span>
@endif
<input id="{{ $name }}"
       type="{{ (isset($type)) ? $type : 'text' }}"
       name="{{ $name }}"

       @if($errors->has($name))
           class="is-invalid"
       @endif

       @if(old($name))
           value="{{ old($name) }}"
       @endif

       @if(!old($name) && isset($value))
            value="{{ $value }}"
       @endif

       @if(isset($attributes))
           @foreach($attributes as $attribute)
               {{ $attribute }}
           @endforeach
       @endif

       @if(isset($placeholder))
           placeholder="{{ $placeholder }}"
       @endif
>