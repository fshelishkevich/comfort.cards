<!DOCTYPE html>
<html>
<head>
    <title>Подтверждение регистрации.</title>
</head>
<body>
<h2>Приветствуем Вас {{$user['login']}}</h2>
<p>
    Вы зарегистрировали ваш E-mail {{$user['email']}} на сайте {{ url('/') }}.
</p>
<p>
    Для того, чтобы завершить процесс регистрации, пожалуйста, подтвердите регистрацию пройдя по ссылке ниже.
</p>
<a href="{{url('user/verify', $user->verifyUser->token)}}">Подтвердить регистрацию</a>
<p>
    Если у вас не получается напрямую перейти по ссылке, вы можете скопировать ссылку ниже и вставить в адресную строку браузера.
</p>
<p>
    <a href="{{url('user/verify', $user->verifyUser->token)}}">{{url('user/verify', $user->verifyUser->token)}}</a>
</p>
</body>
</html>