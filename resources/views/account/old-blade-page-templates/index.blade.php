@extends('account.layout.app')

@section('content')
    <div class="content-container">

        <div class="page-title-stripe">
            <div class="title">Панель управления (Сейчас мы работаем над улучшением сервиса)</div>
        </div>

    @if($user->account)
        <!---- Главная личного кабинета ---->
            <div class="content">
                <div class="standard-block">
                    <div class="sb-t acc-nums tac">
                        <div class="block-title">Информация об аккаунте</div>
                        <div class="block-content">
                            Номер карты: {{ $user->account->card_number }}<br>
                            Тип Вашего аккаунта: {{ $user->account->accountType->name }}<br>
                            Срок действия {{ $user->account->accountType->validity }} дней
                        </div>
                    </div>
                </div>
                <div class="standard-block white-bg">
                    <div>
                        <div class="sb-c acc-nums tac">
                            <div class="acc-sum">{{ $user->account->own_bonus_funds }} баллов</div>
                            <div class="acc-desc">Бонусный счет</div>
                        </div>
                    </div>
                </div>
                <div class="standard-block white-bg">
                    <div>
                        <div class="sb-c acc-nums tac half">
                            <div class="acc-sum">{{ $user->first_lvl_referrals_count ?? 0 }}</div>
                            <div class="acc-desc">Личных рефералов</div>
                        </div>
                        <div class="sb-c acc-nums tac half">
                            <div class="{{--acc-sum--}}">В разработке{{--ххх--}}</div>
                            <div class="acc-desc">Всего рефералов</div>
                        </div>
                    </div>
                </div>
                <div style="clear:both"></div>
                <div class="standard-block">
                    <div class="sb-t acc-nums tac">
                        <div class="block-title">Последние новости</div>
                        <a href="{{ route('account.news') }}" class="block-content">
                            <div>
                                <div class="news-block-title">{{ $news->title }}</div>
                                <div class="news-block-text">
                                    {!! str_limit($news->content, 100) !!}
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
    @else
        <!---- После регистрации ---->

            <div class="content">
                <div class="standard-block white-bg">
                    <div>
                        <div class="card-bind-text">Для начала работы нужно приобрести карту</div>
                        <div class="pack-price card-price">100 грн</div>
                        <div class="tac btn-block">
                            <a href="javascript:void(0)" class="btn blue">Оплатить</a>
                            <br>
                            <br>
                            <a href="javascript:void(0)">Уже есть карта</a>
                            <br>
                        </div>
                    </div>
                </div>
            </div>


            <!---- После нажатия "Уже есть карта" ---->

            <div class="content">
                <div class="standard-block white-bg">
                    <div>
                        <div class="card-bind-text">Привязать карту</div>
                        <div><input class="card-bind-input" type="text" placeholder="8 цифр с вашей карты"></div>
                        <div class="tac btn-block">
                            <a href="javascript:void(0)" class="btn blue btn-buy-card">Привязать</a>
                        </div>
                    </div>
                </div>
            </div>



            <!---- После привязки карты ---->

            <div class="content">
                @foreach($accountTypes as $act)
                    <div class="standard-block white-bg">
                        <div>
                            <div class="pack-title">{{ $act->name }}</div>
                            <div class="pack-desc-stroke">
                                Срок действия {{ $act->validity }} дней
                            </div>
                            <div class="pack-desc-stroke">
                                Бонус за использование {{ $act->min_own_bonus }}-{{ $act->max_own_bonus }}%/день
                            </div>
                            <div class="pack-desc-stroke">
                                {{ $act->referral_bonus }}% от привлеченных пользователей
                            </div>

                            <div class="pack-price">Цена {{ $act->price * 28 }} грн</div>
                            <a href="javascript:void(0)" class="btn-buy-pack">Приобрести</a>
                        </div>
                    </div>
                @endforeach
            </div>
            <!---- END После привязки карты ---->
        @endif
    </div>
    <div class="modal-window-container" id="modal-1">
        <div class="modal-window">
            <div class="modal-title">Оплата пакета<br>BRONZE</div>
            <div class="modal-window-block">
                <p>
                    Вы выбрали пакет BRONZE. Чтобы оплатить нажмите на кнопку ниже.
                </p>
                <button class="modal-window-btn block-btn blue">Оплатить</button>
                <a href="/account.html">Закрыть</a>
            </div>
        </div>
    </div>
@endsection