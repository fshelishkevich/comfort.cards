@extends('account.layout.app')

@section('content')
    <div class="content-container">
        <div class="page-title-stripe">
            <div class="title">Пополнение счета</div>
            <div class="button"><a href="#" class="btn yellow">Улучшить аккаунт</a></div>
        </div>

        <div class="content">
            <div class="full-block white-bg">
                <div class="sb-c text">
                    <h2>Выберите способ оплаты</h2>
                    <input class="hide" id="hd-1" type="checkbox">
                    <label for="hd-1" class="deposit-method-link">Банковская карта</label>
                    <a href="#" class="deposit-method-link">Терминал</a>
                    <a href="#" class="deposit-method-link">Perfect money / WebMoney</a>
                </div>
            </div>
        </div>
    </div>
@endsection