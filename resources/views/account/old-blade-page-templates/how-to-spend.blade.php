@extends('account.layout.app')

@section('content')
    <div class="content-container">
        <div class="page-title-stripe">
            <div class="title">Куда потратить</div>
            <div class="button"><a href="#" class="btn yellow">Улучшить аккаунт</a></div>
        </div>

        <div class="content">
            <div class="double-block">
                <a href="#" class="sb-t add acc-nums tac">
                    <img src="https://pp.vk.me/c633525/v633525991/2f91a/ubzosqyyZ20.jpg" alt="">
                </a>
            </div>
            <div class="standard-block">
                <a href="#" class="sb-t add acc-nums tac">
                    <img src="{{ asset('images/ad.png') }}" alt="">
                </a>
            </div>
            <div class="standard-block">
                <a href="#" class="sb-t add acc-nums tac">
                    <img src="{{ asset('images/ad.png') }}" alt="">
                </a>
            </div>
            <!--
            <div class="standard-block ad">
                <img src="/img/ad.png">
            </div>
            -->
        </div>
    </div>
@endsection