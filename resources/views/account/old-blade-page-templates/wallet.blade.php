@extends('account.layout.app')

@section('content')
    <div class="content-container">
        <div class="page-title-stripe">
            <div class="title">Кошелек</div>
            <div class="button"><a href="#" class="btn yellow">Улучшить аккаунт</a></div>
        </div>
        <div class="content text-content">
            <div class="half-block white-bg">
                <div>
                    <div class="sb-c acc-nums tac">
                        <div class="acc-sum">$1.400</div>
                        <div class="acc-desc">Основной счет</div>
                    </div>
                </div>
            </div>
            <div class="half-block white-bg">
                <div>
                    <div class="sb-c acc-nums tac">
                        <div class="acc-sum">$400</div>
                        <div class="acc-desc">Бонусный счет</div>
                    </div>
                </div>
            </div>
            <div class="half-block">
                <a href="{{ route('account.depositFunds') }}" class="block-btn green">
                    Пополнить кошелек
                </a>
            </div>
            <div class="half-block">
                <a href="{{ route('account.withdrawFunds') }}" class="block-btn blue">
                    Вывести с кошелька
                </a>
            </div>
            <div class="full-block">
                <div class="sb-t acc-nums tac">
                    <div class="block-title">Статистика кошелька
                        <div class="right-text"><a href="{{ route('account.walletHistory') }}">История платежей</a></div>
                    </div>
                    <div class="block-content">Тест</div>
                </div>
            </div>
        </div>
    </div>
@endsection