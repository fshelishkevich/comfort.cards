@extends('account.layout.app')

@section('content')
    <div class="content-container">
        <div class="page-title-stripe">
            <div class="title">Вывод средств</div>
            <div class="button"><a href="#" class="btn yellow">Улучшить аккаунт</a></div>
        </div>
        <div class="content">
            <div class="full-block white-bg">
                <div class="sb-c text">
                    <h2>Выберите способ вывода</h2>
                    <a href="#" class="deposit-method-link">Карта "Comfort Card"</a>
                    <a href="#" class="deposit-method-link">Банковская карта</a>
                    <a href="#" class="deposit-method-link">Perfect money / WebMoney</a>
                </div>
            </div>
        </div>
    </div>
@endsection