@extends('account.layout.app')

@section('content')
    <div class="content-container">
        <div class="page-title-stripe">
            <div class="title">История платежей</div>
            <div class="button"><a href="#" class="btn yellow">Улучшить аккаунт</a></div>
        </div>
        <div class="content">
            <div class="full-block white-bg">
                <div class="sb-c text">
                    <h2>История платежей</h2>
                    <div class="wallet-history-t table-style">
                        <div class="ts-tr header-row">
                            <div class="ts-tc">Дата и время</div>
                            <div class="ts-tc">Сумма</div>
                            <div class="ts-tc">Покупка</div>
                            <div class="ts-tc">Место</div>
                        </div>
                        <div class="ts-tr">
                            <div class="ts-tc">Дата и время</div>
                            <div class="ts-tc">Сумма</div>
                            <div class="ts-tc">Покупка</div>
                            <div class="ts-tc">Место</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection