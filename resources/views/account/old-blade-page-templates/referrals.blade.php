@extends('account.layout.app')

@section('content')
    <div class="content-container">
        <div class="page-title-stripe">
            <div class="title">Рефералы</div>
            <div class="button"><a href="#" class="btn yellow">Улучшить аккаунт</a></div>
        </div>
        <div class="content">
            <div class="full-block white-bg">
                <div class="sb-c text">
                    <h2>Ваши рефералы</h2>
                    <p>
                        Равным образом начало повседневной работы по формированию позиции позволяет выполнять важные задания по разработке позиций, занимаемых участниками в отношении поставленных задач. Товарищи! консультация с широким активом позволяет оценить значение модели развития. Не следует, однако забывать, что рамки и место обучения кадров требуют определения и уточнения систем массового участия.
                    </p>
                </div>
            </div>
            <div class="half-block white-bg">
                <div>
                    <div class="sb-c acc-nums tac">
                        <div class="acc-sum">32</div>
                        <div class="acc-desc">Прямых рефералов</div>
                    </div>
                </div>
            </div>
            <div class="half-block white-bg">
                <div>
                    <div class="sb-c acc-nums tac">
                        <div class="acc-sum">10</div>
                        <div class="acc-desc">Арендованых рефералов</div>
                    </div>
                </div>
            </div>
            <div class="full-block">
                <div class="sb-t acc-nums tac">
                    <div class="block-title">График доходности</div>
                    <div class="block-content">Тест</div>
                </div>
            </div>
            <div class="full-block">
                <div class="sb-t acc-nums tac">
                    <div class="block-title">График активности рефералов</div>
                    <div class="block-content">Тест</div>
                </div>
            </div>
        </div>
    </div>
@endsection