<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Account</title>
    <link rel="stylesheet" href="css/style.css">
    <link href='https://fonts.googleapis.com/css?family=Comfortaa:400,300,700&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
</head>
<body>
    <header class="account">
        <div class="top-table">
           
            <a href="#" class="logo-img"></a>
            <a href="#" class="logo-title">ComfortCard+</a>
            
            <div class="header-toolbar"><a href="#">Выход</a></div>
            
        </div>
    </header>
    <div class="account-content">
        <div class="menu">
            <div class="profile">
                <div class="profile-img">
                    <img src="img/profile.png" alt="">
                </div>
                <div class="profile-info">
                    <div class="name">Ramil Juhad</div>
                    <div class="exit"><a href="#">Выход</a></div>
                </div>
            </div>
            <ul class="cats">
                <li>
                    <div class="cat-title">Панель управления</div>
                    <ul class="subcats">
                        <a href="account.html"><li>Главная</li></a>
                        <a href="account-type.html"><li>Пакеты</li></a>
                    </ul>
                </li>
                <li>
                    <div class="cat-title">Аккаунт</div>
                    <ul class="subcats">
                        <a href="profile.html" class="active"><li>Личный кабинет</li></a>
                        <a href="referrals.html"><li>Рефералы</li></a>
                        <a href="statistic.html"><li>Статистика</li></a>
                    </ul>
                </li>
                <li>
                    <div class="cat-title">Финансы</div>
                    <ul class="subcats">
                        <a href="wallet.html"><li>Кошелек</li></a>
                        <a href="deposit-funds.html"><li>Пополнить счет</li></a>
                        <a href="withdraw-funds.html"><li>Вывести средства</li></a>
                        <a href="wallet-history.html"><li>История платежей</li></a>
                        <a href="how-to-spend.html"><li>Куда потратить?</li></a>
                    </ul>
                </li>
            </ul>
        </div>
        <div class="content-container">
            <div class="page-title-stripe">
                <div class="title">Настройки</div>
                <div class="button"><a href="#" class="btn yellow">Улучшить аккаунт</a></div>
            </div>
            <div class="content text-content">
                <h2>Настройки аккаунта</h2>
                
            </div>
        </div>
    </div>
</body>

</html>