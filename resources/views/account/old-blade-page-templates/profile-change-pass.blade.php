@extends('account.layout.app')

@section('content')
    <div class="content-container">
        <div class="page-title-stripe">
            <div class="title">Изменение данных</div>
            <div class="button"><a href="#" class="btn yellow">Улучшить аккаунт</a></div>
        </div>
        <div class="content">
            <div class="full-block white-bg">
                <div class="sb-c text">
                    <div class="table-style profile-t">
                        <div class="ts-tc photo">
                            <div class="profile-img-big">
                                <img src="{{ url('images/profile.png') }}" alt="">
                            </div>
                            <div class="name-stroke">Ramil Juhad</div>
                        </div>
                        <div class="ts-tc info"><!--
                                    -->
                            <div class="info-stroke half">
                                <div class="info-stroke-title">Имя</div>
                                <input type="text" class="info-stroke-content" placeholder="Ramil">
                            </div><!--
                                    -->
                            <div class="info-stroke half">
                                <div class="info-stroke-title">Фамилия</div>
                                <input type="text" class="info-stroke-content" placeholder="Juhad">
                            </div><!--
                                    -->
                            <div class="info-stroke half">
                                <div class="info-stroke-title">Страна</div>
                                <select class="info-stroke-content">
                                    <option default>Украина</option>
                                    <option>Россия</option>
                                </select>
                            </div><!--
                                    -->
                            <div class="info-stroke half">
                                <div class="info-stroke-title">Город</div>
                                <input type="" class="info-stroke-content" placeholder="Киев">
                            </div><!--
                                    -->
                            <div class="info-stroke half">
                                <div class="info-stroke-title">Дата рождения</div>
                                <select class="birth-date-small">
                                    <option default>28</option>
                                </select>
                                <select class="birth-date-big">
                                    <option default>May</option>
                                </select>
                                <select class="birth-date-small">
                                    <option default>1996</option>
                                </select>
                            </div><!--
                                    -->
                            <div class="info-stroke half">
                                <div class="info-stroke-title">E-mail</div>
                                <input type="" class="info-stroke-content" placeholder="address@mail.com">
                            </div><!--
                                    -->
                            <div class="info-stroke half">
                                <div class="info-stroke-title">Телефон</div>
                                <input type="" class="info-stroke-content" placeholder="+38 (093) 286 27 32">
                            </div><!--
                                    -->
                            <div class="info-stroke half">
                                <div class="info-stroke-title">Номер карты</div>
                                <input type="" class="info-stroke-content" placeholder="2458 4587 9874 5698">
                            </div><!--
                                    -->
                            <div class="info-stroke half">
                                <div class="info-stroke-title">Пароль</div>
                                <input type="password" class="info-stroke-content" placeholder="Старый пароль">
                            </div><!--
                                    -->
                            <div class="info-stroke half">
                                <div class="info-stroke-title">&nbsp;</div>
                                <input type="password" class="info-stroke-content" placeholder="Новый пароль">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="full-block">
                <a href="profile.html" class="block-btn green">
                    Сохранить
                </a>
            </div>
        </div>
    </div>
@endsection