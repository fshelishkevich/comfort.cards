@extends('account.layout.app')

@section('content')
    <div class="content-container">
        <div class="page-title-stripe">
            <div class="title">Пакеты</div>
            <div class="button"><a href="#" class="btn yellow">Улучшить аккаунт</a></div>
        </div>
        <div class="content">
            <div class="full-block white-bg">
                <div class="sb-c text">
                    <div class="table-style pack-t">
                        <div class="ts-tr">
                            <div class="ts-tc">Тип партнерства</div>
                            <div class="ts-tc active">Free</div>
                            <div class="ts-tc">Silver</div>
                            <div class="ts-tc">Gold</div>
                            <div class="ts-tc">Platinum</div>
                        </div>
                        <div class="ts-tr">
                            <div class="ts-tc">Цена</div>
                            <div class="ts-tc price active">0 <div>грн/мес</div></div>
                            <div class="ts-tc price">100 <div>грн/мес</div></div>
                            <div class="ts-tc price">300 <div>грн/мес</div></div>
                            <div class="ts-tc price">3000 <div>грн/<span>год</span></div></div>
                        </div>
                        <div class="ts-tr">
                            <div class="ts-tc">Бонус</div>
                            <div class="ts-tc active">-</div>
                            <div class="ts-tc">-</div>
                            <div class="ts-tc">-</div>
                            <div class="ts-tc">Карта <span class="blue">"Comfort Card"</span><br>и <span class="blue">5%</span> скидки</div>
                        </div>
                        <div class="ts-tr">
                            <div class="ts-tc">Рефералы</div>
                            <div class="ts-tc active">0</div>
                            <div class="ts-tc">1</div>
                            <div class="ts-tc">3</div>
                            <div class="ts-tc">3</div>
                        </div>
                        <div class="ts-tr">
                            <div class="ts-tc">Лимит рефералов</div>
                            <div class="ts-tc active">20</div>
                            <div class="ts-tc">100</div>
                            <div class="ts-tc">200</div>
                            <div class="ts-tc">300</div>
                        </div>
                        <div class="ts-tr">
                            <div class="ts-tc">Онлайн консультация</div>
                            <div class="ts-tc active">+</div>
                            <div class="ts-tc">+</div>
                            <div class="ts-tc">+</div>
                            <div class="ts-tc">+</div>
                        </div>
                        <div class="ts-tr">
                            <div class="ts-tc"></div>
                            <div class="ts-tc active"></div>
                            <div class="ts-tc link-tc"><a href="#" class="blue upgrade-btns">Улучшить</a></div>
                            <div class="ts-tc link-tc"><a href="#" class="blue upgrade-btns">Улучшить</a></div>
                            <div class="ts-tc link-tc"><a href="#" class="blue upgrade-btns">Улучшить</a></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection