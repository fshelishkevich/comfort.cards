<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Восстановление пароля</title>
    <link rel="stylesheet" href="/css/style.css">
    <link href='https://fonts.googleapis.com/css?family=Comfortaa:400,300,700&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
</head>
<body>
    <header class="account">
        <div class="top-table tac">
           
            <a href="#" class="logo-img"></a>
            <a href="#" class="logo-title">ComfortCard+</a>
            
        </div>
    </header>
    <div class="account-content">
        <div class="content-container">
            <div class="content">
                <div class="logreg-form tac">
                    <form action="">
                        <h1>Восстановление пароля</h1>
                        <input type="text" placeholder="Логин или E-mail">
                        <button>Восстановить</button>
                        <div class="links tac">
                            <a href="javascript:void(0)">Вход</a> | <a href="javascript:void(0)">Регистрация</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</body>

</html>