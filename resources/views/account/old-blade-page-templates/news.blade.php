@extends('account.layout.app')

@section('content')
    <div class="content-container">

        <div class="page-title-stripe">
            <div class="title">Панель управления (Сейчас мы работаем над улучшением сервиса)</div>
        </div>

        <!---- Это для страницы новостей ---->
        <div class="content">
            @foreach($news as $item)
                <div class="double-block">
                    <div class="sb-t acc-nums tac">
                        <div class="block-title">Последние новости</div>
                        <div class="block-content">
                            {{--<div class="news-block-img"><img src="/images/news-post-1.jpg"></div>--}}
                            <div>
                                <div class="news-block-title">{{ $item->title }}</div>
                                <div class="news-block-text">
                                    {!! $item->content !!}
                                </div>
                                <div>
                                    <hr>
                                    {{ $item->created_at }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        <!---- /Это для страницы новостей ---->

    </div>
@endsection