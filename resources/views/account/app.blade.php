<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Личный кабинет</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="stylesheet" href="{{ asset('css/account.css') }}">
</head>
<body>
@include('account.header')
<div id="app" class="account-content">
    @include('account.sidebar')
    <div class="content-container">

        <div class="page-title-stripe">
            <div class="title">Панель управления (Сейчас мы работаем над улучшением сервиса)</div>
        </div>

        <router-view></router-view>
    </div>
</div>
<script src="{{ asset('js/account.js') }}"></script>
</body>
</html>