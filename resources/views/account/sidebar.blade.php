<div class="menu">
    <div class="profile">
        <div class="profile-img">
            <img src="{{ asset('/images/userphoto.svg') }}" width="35px" alt="">
        </div>
        <div class="profile-info">
            <div class="name">Добро пожаловать!</div>
            <div class="exit">@include('features.buttons.logout')</div>
        </div>
    </div>
    <ul class="cats">
        <li>
            <div class="cat-title">Панель управления</div>
            <ul class="subcats">
                <router-link to="/" exact><li>Главная</li></router-link>
                <router-link to="/news"><li>Новости</li></router-link>

                {{--<a href="{{ route('account.type') }}"
                   class="{{ (Route::currentRouteName() == 'account.type') ? 'active' : '' }}">
                    <li>Пакеты</li>
                </a>--}}
            </ul>
        </li>
{{--<li>
    <div class="cat-title">Аккаунт</div>
    <ul class="subcats">
        <a href="{{ route('account.profile') }}"
           class="{{ (Route::currentRouteName() == 'account.profile') ? 'active' : '' }}">
            <li>Профиль</li>
        </a>
        <a href="{{ route('account.referrals') }}"
           class="{{ (Route::currentRouteName() == 'account.referrals') ? 'active' : '' }}">
            <li>Рефералы</li>
        </a>
        <a href="{{ route('account.statistic') }}"
           class="{{ (Route::currentRouteName() == 'account.statistic') ? 'active' : '' }}">
            <li>Статистика</li>
        </a>
    </ul>
</li>
<li>
    <div class="cat-title">Финансы</div>
    <ul class="subcats">
        <a href="{{ route('account.wallet') }}"
           class="{{ (Route::currentRouteName() == 'account.wallet') ? 'active' : '' }}">
            <li>Кошелек</li>
        </a>
        <a href="{{ route('account.depositFunds') }}"
           class="{{ (Route::currentRouteName() == 'account.depositFunds') ? 'active' : '' }}">
            <li>Пополнить счет</li>
        </a>
        <a href="{{ route('account.withdrawFunds') }}"
           class="{{ (Route::currentRouteName() == 'account.withdrawFunds') ? 'active' : '' }}">
            <li>Вывести средства</li>
        </a>
        <a href="{{ route('account.walletHistory') }}"
           class="{{ (Route::currentRouteName() == 'account.walletHistory') ? 'active' : '' }}">
            <li>История платежей</li>
        </a>
        <a href="{{ route('account.howToSpend') }}"
           class="{{ (Route::currentRouteName() == 'account.howToSpend') ? 'active' : '' }}">
            <li>Куда потратить?</li>
        </a>
    </ul>
</li>
--}}
</ul>
</div>