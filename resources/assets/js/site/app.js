import Vue from 'vue'

window.Vue = Vue

Vue.component('main-page', require('./pages/MainPage'))

new Vue({
    el: '#app',
})