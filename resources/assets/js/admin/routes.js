import VueRouter from 'vue-router'
import Vue from "vue";
Vue.use(VueRouter)

let routes = [
    {
        path: '/',
        component: require('./views/Dashboard')
    },
    {
        path: '/users',
        component: require('./views/cruds/Users')
    },
    {
        path: '/account-types',
        component: require('./views/cruds/AccountTypes')
    },
    {
        path: '/news',
        component: require('./views/cruds/News')
    },
    {
        path: '/transaction-types',
        component: require('./views/cruds/TransactionTypes')
    },
    {
        path: '/orders',
        component: require('./views/Orders')
    }
]

export default new VueRouter({
    routes,
    linkActiveClass: 'is-active',
})