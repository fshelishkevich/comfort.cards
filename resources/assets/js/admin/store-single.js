import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const storeSingle = new Vuex.Store({
    state: {
        count: 0,
        form: {
            isOpen: false,
            data: 'test'
        },
    },

    mutations: {
        openModalForm(state){
            state.form.isOpen = true
        },

        closeModalForm(state){
            state.form.isOpen = false
        }
    },
})

export default storeSingle