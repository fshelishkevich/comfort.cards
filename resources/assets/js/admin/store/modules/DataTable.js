const state = {
    isLoading: true,
    apiUrl: '',
    items: [],
    links: {},
    meta: {}
}

const mutations = {
    setApiUrl(state, url){
        state.apiUrl = url
    },

    setItems(state, url)
    {
        let page_url = url || state.apiUrl
        state.isLoading = true
        axios.get(page_url)
            .then(response => {
                state.isLoading = false
                state.items = response.data.data
                state.links = response.data.links
                state.meta = response.data.meta
            })
            .catch(error => {
                if (error.response.status === 401) {
                    window.location.replace("/login")
                }
            })
    }
}

export default {
    namespaced: true,
    state,
    mutations,
}