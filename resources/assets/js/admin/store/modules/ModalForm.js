const state = {
    isOpen: false,
    isSubmitting: false,
    title: '',
    fields: Array,
    apiUrl: '',
    inputData: {},
    method: '',
    action: '',
    errors: '',
}

const actions = {
    openModalForm(context, state, data) {
        context.commit('setFormFields', state, data)
        context.commit('setFormTitle', state, data)
        context.commit('setApiUrl', state, data)
        context.commit('setMethod', state, data)
        context.commit('openForm')
    },

    closeModalForm(context) {
        context.commit('closeForm')
        context.commit('resetInputData')
        context.commit('resetErrors')
    },

    submitForm({commit, state}) {
        commit('resetErrors', state)
        state.isSubmitting = true
        return new Promise((resolve, reject) => {
            axios[state.method](state.apiUrl, state.inputData)
                .then(response => {
                    state.isSubmitting = false
                    state.isOpen = false

                    // @FIXME make it through method
                    state.inputData = {}
                    state.fields = {}
                    resolve(response)
                })
                .catch(({response}) => {
                    if (response.status === 401) {
                        window.location.replace("/login")
                    }
                    state.isSubmitting = false
                    state.errors = response.data.errors
                    console.log(response.data.errors)

                    reject(response)
                })
        })
    }
}

const mutations = {
    openForm(state) {
        state.isOpen = true
    },

    closeForm(state) {
        state.isOpen = false
    },

    updateInputData(state, data) {
        state.inputData[data.name] = data.value
        for (let field in state.fields) {
            if (field === data.name) {
                state.fields[field].value = data.value
            }
        }
    },

    submitForm(state) {
        state.isSubmitting = true
        return new Promise((resolve, reject) => {
            axios[state.method](state.apiUrl, state.inputData)
                .then(response => {
                    state.isSubmitting = false
                    state.isOpen = false
                    resolve(response)
                })
                .catch(({response}) => {
                    if (response.status === 401) {
                        window.location.replace("/login")
                    }
                    state.isSubmitting = false
                    state.errors = response.data.errors
                    console.log(response.data.errors)

                    reject(response)
                })
        })
    },

    resetErrors(state) {
        state.errors = {}
    },

    resetInputData(state) {
        state.inputData = {}
        state.fields = {}
    },

    setFormTitle(state, data) {
        if (data.action === 'edit') {
            state.title = data.itemName + ' (Редактирование)'
        } else {
            state.title = 'Новый ' + data.itemName.toLowerCase()
        }
    },

    setApiUrl(state, data) {
        state.apiUrl = data.apiUrl
        state.apiUrl += data.action === 'edit' ? '/' + data.item.id : ''
    },

    setMethod(state, data) {
        state.method = 'post'
        if (data.action === 'edit') {
            state.method = 'put'
        }
    },

    setFormFields(state, data) {
        state.fields = data.fields
        for (let fieldName in state.fields) {
            let field = state.fields[fieldName]

            state.inputData[field.name] = ''

            if (data.action === 'edit') {
                state.inputData[field.name] = field.value = data.item[field.name]
            }

            field.componentName = 'text-field'
            switch (field.type) {
                case 'integer':
                    field.additionalInfo = 'Целое число'
                    break
                case 'float':
                    field.additionalInfo = "Дробное число через знак '.'"
                    break
                case 'select':
                    field.componentName = 'select-field'
                    break
                case 'editor':
                    field.componentName = 'editor-field'
                    break

                default:
                    break
            }
        }
    },
}

const getters = {
    hasError: state => fieldName => {
        return state.errors.hasOwnProperty(fieldName)
    },

    getError: state => fieldName => {
        if (state.errors[fieldName]) {
            return state.errors[fieldName][0]
        }
    }
}

export default {
    namespaced: true,
    state,
    actions,
    mutations,
    getters
}