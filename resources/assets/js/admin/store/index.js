import Vue from 'vue'
import Vuex from 'vuex'
import modalForm from './modules/ModalForm'
import dataTable from './modules/DataTable'

Vue.use(Vuex)

export default new Vuex.Store({
    modules: {
        modalForm, dataTable
    },
})