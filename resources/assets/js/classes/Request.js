export class Request {
    constructor(){
        this.isLoading = false
    }

    get(url) {
        this.isLoading = true
        let self = this
        return new Promise((resolve, reject) => {
            axios.get(url)
                .then(response => {
                    self.isLoading = false
                    resolve(response.data)
                })
                .catch(error => {
                    self.isLoading = false
                    if (error.response.status === 401) {
                        window.location.replace("/login")
                    }

                    reject(error)
                })
        })
    }

    post(url, data) {
        this.isLoading = true
        let self = this
        return new Promise((resolve, reject) => {
            axios.post(url, data)
                .then(response => {
                    self.isLoading = false
                    resolve(response.data)
                })
                .catch(error => {
                    self.isLoading = false
                    if (error.response.status === 401) {
                        window.location.replace("/login")
                    }

                    reject(error)
                })
        })
    }
}