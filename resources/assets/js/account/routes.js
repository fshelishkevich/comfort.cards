import VueRouter from 'vue-router'
import Vue from 'vue'
Vue.use(VueRouter)

let routes = [
    {
        path: '/',
        component: require('./views/Index')
    },
    {
        path: '/news',
        component: require('./views/News')
    }
]

export default new VueRouter({
    routes,
    linkActiveClass: 'active',
})