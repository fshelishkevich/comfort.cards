<?php


namespace Tests\Extensions;


trait VisitExtension
{
    function visitRegisterRefUrl($token)
    {
        $this->visit('/register?ref=' . $token);
    }
}