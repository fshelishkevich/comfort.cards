<?php


namespace Tests\Extensions;


use App\User;

trait FormInputExtension
{
    /*
     * ---------------------------------------------
     *		Helpers
     * ---------------------------------------------
     */
    private function fillRegisterFormAndGetRegisteredUser(string $login, string $email, string $password, string $sponsorLogin = null)
    {
        $this->visit('/register');
        $this->fillRegisterForm($login, $email, $password, $sponsorLogin);

        return User::whereLogin($login)->first();
    }

    private function fillRegisterForm(string $login = 'login', string $email = 'mail@mail.com', string $password = 'secret', string  $sponsorLogin = null)
    {
        $this->type($login, 'login');
        $this->type($email, 'email');
        $this->type($password, 'password');
        $this->type($password, 'password_confirmation');
        if($sponsorLogin){
            $this->type($sponsorLogin, 'sponsor_login');
        }
        $this->press('Регистрация');
    }

    private function fillLoginForm(string $login, string $password)
    {
        $this->type($login, 'login');
        $this->type($password, 'password');
        $this->press('Войти');
    }
}