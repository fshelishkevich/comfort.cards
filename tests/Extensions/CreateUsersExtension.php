<?php


namespace Tests\Extensions;


use App\User;
use Hash;

trait CreateUsersExtension
{
    function createUsers($verified = false, $amount = 1)
    {
        $users = [];
        foreach (range(1, $amount) as $index){
            $login = 'login' . $index;
            $email = "mail{$index}@mail.com";
            $password = 'secret' . $index;

            $users[] = User::create( [
                'login'          => $login,
                'email'          => $email,
                'password'       => Hash::make( $password ),
                'sponsor_id'     => 1,
                'verified' => $verified,
            ] );
        }

        if(count($users) > 1){
            return $users;
        }

        return $users[0];
    }
}