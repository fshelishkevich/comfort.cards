<?php

namespace Tests\Feature\Auth;

use Tests\Extensions\FormInputExtension;
use Tests\TestCase;

class RegisterValidationTest extends TestCase
{
	use FormInputExtension;

    /** @test */
	function login_must_be_at_least_4_characters()
	{
		$this->visit('/register');
		$this->fillRegisterForm('log');

		$this->seeText('Поле должно содержать как минимум 4 символов.');
	}

	/** @test */
	function login_must_contain_only_english_characters()
	{
		$this->visit('/register');
		$this->fillRegisterForm('РусскийЛогин');

		$this->seeText('Поле может содержать только символы латиницы, цифры и подчеркивание.');
	}
}
