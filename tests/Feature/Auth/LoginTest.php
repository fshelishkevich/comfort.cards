<?php

namespace Tests\Feature\Auth;

use App\VerifyUser;
use Illuminate\Support\Facades\Mail;
use Tests\Extensions\FormInputExtension;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class LoginTest extends TestCase
{
    use FormInputExtension;

    protected function setUp()
    {
        parent::setUp();
        Mail::fake();
    }

    /** @test */
    function user_cant_login_while_not_verified()
    {
        $this->visit('/register');
        $this->fillRegisterForm('login', 'some@email.com', 'secret');

        $this->visit('login');
        $this->fillLoginForm('login', 'secret');

        $this->seeText('Вам необходимо пройти подтверждение регистрации');
    }

    /** @test */
    function verified_user_can_log_id()
    {
        $user = $this->fillRegisterFormAndGetRegisteredUser('login', 'mail@sdf.sdf', 'secret');
        $verifyToken = VerifyUser::whereUserId($user->id)->first()->token;

        $this->visit(route('user.verify', $verifyToken));

        $this->fillLoginForm('login', 'secret');
        $this->seePageIs(route('account.index'));
    }
}
