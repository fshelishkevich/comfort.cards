<?php

namespace Tests\Feature\Auth;

use App\Mail\VerifyMail;
use App\User;
use App\VerifyUser;
use Hash;
use Illuminate\Support\Facades\Mail;
use Tests\Extensions\CreateUsersExtension;
use Tests\Extensions\FormInputExtension;
use Tests\Extensions\VisitExtension;
use Tests\TestCase;

class RegisterTest extends TestCase
{
    use FormInputExtension;
    use CreateUsersExtension;
    use VisitExtension;

    protected function setUp()
    {
        parent::setUp();
        Mail::fake();
    }

    /** @test */
    function that_after_register_user_sees_notify_about_mail_verify()
    {
        $this->visit( '/register' );
        $this->fillRegisterForm( 'login', 'some@email.com', 'secret' );

        $this->seeText( 'Мы отправили Вам письмо' );
    }

    /** @test */
    function check_that_email_sended_to_registered_user()
    {
        $user = $this->fillRegisterFormAndGetRegisteredUser( 'login', 'mail@sdf.sdf', 'secret' );

        Mail::assertSent( VerifyMail::class, function ( $mail ) use ( $user ) {
            return $mail->hasTo( $user->email );
        } );
    }

    /** @test */
    function that_registered_user_can_be_verified()
    {
        $user        = $this->fillRegisterFormAndGetRegisteredUser( 'login', 'mail@sdf.sdf', 'secret' );
        $verifyToken = VerifyUser::whereUserId( $user->id )->first()->token;

        $this->visit( route( 'user.verify', $verifyToken ) );

        $this->seeText( 'Вы успешно прошли подтверждение регистрации. Теперь можете войти в приложение.' );
    }

    /** @test */
    function see_non_verified_sponsors_warning_if_used_appropriate_not_verified_sponsors_ref_link()
    {
        /** @var User $nonVerifiedUser */
        $nonVerifiedUser = $this->createUsers();

        $this->visitRegisterRefUrl( $nonVerifiedUser->login );

        $this->seeText( __( 'warnings.not-verified' ) );
    }

    /** @test */
    function check_message_about_wrong_referral_url()
    {
        $this->visitRegisterRefUrl( 'wrong_token' );

        $this->seeText( __( 'warnings.referral_link_broken' ) );
    }

    /** @test */
    function check_wrong_typed_sponsor_login()
    {
        $this->fillRegisterFormAndGetRegisteredUser( 'login', 'email@mail.com', 'secret', 'wrong_sponsor' );

        $this->seeText( __( 'warnings.user_not_exist' ) );
    }

    /** @test */
    function check_not_verified_typed_sponsor_login()
    {
        $user = $this->createUsers();

        $this->fillRegisterFormAndGetRegisteredUser( 'refLogin', 'ref@email.com', 'secret', $user->login );

        $this->seeText(__('warnings.typed_sponsor_login_not_verified'));
    }

    /** @test */
    function check_default_sponsor_id_equal_one()
    {
        $user = $this->fillRegisterFormAndGetRegisteredUser('login', 'mail@mail.cc', 'secret');

        $this->assertTrue(1 == $user->sponsor_id);
    }

    /** @test */
    function correct_ref_link_does_not_show_warnings()
    {
        /** @var User $verifiedUser */
        $verifiedUser = $this->createUsers(true);

        $this->visitRegisterRefUrl($verifiedUser->referral_token);

        $this->dontSeeText(__('warnings.referral_link_broken'));
        $this->dontSeeText(__('warnings.not-verified'));
    }

    /** @test */
    function check_that_ref_link_fills_verified_sponsor_login()
    {
        /** @var User $verifiedUser */
        $verifiedUser = User::create([
            'login' => 'someVeryUniqueLoginToCheck',
            'email' => 'some@mail.com',
            'password' => Hash::make('secret'),
            'verified' => true,
            'sponsor_id' => 1,
        ]);

        $this->visitRegisterRefUrl($verifiedUser->login);

        $this->see($verifiedUser->login);
    }

    /** @test */
    function registered_user_with_typed_correct_sponsor_become_his_referral()
    {
        $users = $this->createUsers(true, 5);
        $sponsor = end($users);

        $this->visit('register');
        $login = 'newReferral';
        $this->fillRegisterForm($login, 'newReferral@mail.com', 'secret', $sponsor->login);
        $registeredReferral = User::whereLogin($login)->first();

        $this->assertTrue($sponsor->id == $registeredReferral->sponsor_id, 'Sponsor id is not equal');
    }

    /** @test */
    function registered_user_that_used_correct_sponsor_link_is_becomes_his_referral()
    {
        $users = $this->createUsers(true, 3);

        /** @var User $verifiedUser */
        $sponsor = end($users);

        // Ensure that the sponsor_id is not the default
        $this->assertTrue(1 != $sponsor->id);

        $this->visitRegisterRefUrl($sponsor->login);

        $login = 'newReferral';
        $this->fillRegisterForm($login, 'some@email.com', 'secret');

        $registeredReferral = User::whereLogin($login)->first();

        $this->assertTrue($sponsor->id == $registeredReferral->sponsor_id, 'Sponsor id is not equal');
    }

    /** @test */
    function see_correct_referral_link_in_account()
    {
        /** @var User $registeredUser */
        $registeredUser = $this->fillRegisterFormAndGetRegisteredUser('login', 'mail@mail.com', 'secret');
        $registeredUser->verified = true;
        $registeredUser->save();

        $this->actingAs($registeredUser);
        $this->visit(route('account.index'));

        $this->seeText(url('register?ref=' . $registeredUser->referral_token));
    }
}
