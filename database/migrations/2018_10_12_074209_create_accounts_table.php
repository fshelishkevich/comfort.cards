<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accounts', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('account_type_id');
            $table->string('card_number', 8);
            $table->dateTime('has_been_paid_at');
            $table->dateTime('expiry_date');
            $table->boolean('is_active')->comment('Active when user can reach bonuses');
            $table->decimal('own_funds', 10, 2)->default(0);
            $table->decimal('own_bonus_funds', 10, 4)->default(0);
            $table->decimal('referral_bonus_funds', 10, 4)->default(0);


            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade');

            $table->foreign('account_type_id')
                ->references('id')->on('account_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accounts');
    }
}
