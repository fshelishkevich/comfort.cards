<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccountTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('account_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->unsignedInteger('validity')
                ->comment('Amount of a days after which the account will be disabled');

            $table->unsignedInteger('price');
            $table->float('min_own_bonus');
            $table->float('max_own_bonus');
            $table->unsignedInteger('referral_bonus')
                ->comment('Bonus for the first lvl referrals');

            $table->unsignedInteger('additional_referral_bonus')->nullable()
                ->comment('Bonus for the referrals from 2d till 10th level');

            $table->unsignedInteger('total_amount_own_bonus')
                ->comment('Total money that can be received on this type of account as the own bonus');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('account_types');
    }
}
