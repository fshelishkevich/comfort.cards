<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablesForTransactions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction_types', function(Blueprint $t){
            $t->increments('id');
            $t->string('title');
        });

        Schema::create('transactions', function (Blueprint $t){
            $t->increments('id');
            $t->unsignedInteger('user_id');
            $t->unsignedInteger('account_id');
            $t->unsignedInteger('transaction_type_id');
            $t->decimal('amount', 10, 4)
                ->comment('Total money sum of the transaction');
            $t->timestamps();

            $t->foreign('user_id')->references('id')->on('users');
            $t->foreign('account_id')->references('id')->on('accounts');
            $t->foreign('transaction_type_id')->references('id')->on('transaction_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
        Schema::dropIfExists('transaction_types');
    }
}
