<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablesForOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_statuses', function(Blueprint $t){
            $t->increments('id');
            $t->string('title');
        });

        Schema::create('orders', function(Blueprint $t){
            $t->increments('id');
            $t->unsignedInteger('user_id');
            $t->unsignedInteger('account_type_id');
            $t->unsignedInteger('order_status_id');
            $t->string('random_id', 20)->unique()
                ->comment('Unique random id that will be used in payment APIs');
            $t->timestamps();

            $t->foreign('user_id')->references('id')->on('users');
            $t->foreign('account_type_id')->references('id')->on('account_types');
            $t->foreign('order_status_id')->references('id')->on('order_statuses');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
        Schema::dropIfExists('order_statuses');
    }
}
