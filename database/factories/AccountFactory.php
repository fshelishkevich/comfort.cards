<?php

use App\Models\Account\Account;
use Carbon\Carbon;
use Faker\Generator as Faker;

$factory->define(Account::class, function (Faker $faker) {
    $hasBeenPaid = Carbon::now()->subDays(45);

    return [
        'account_type_id' => mt_rand(1, 4),
        'has_been_paid_at' => Carbon::now(),
        'expiry_date' => Carbon::now(),
        'is_active' => 1,
        'own_bonus_funds' => 0,
//        'own_bonus_funds' => $faker->randomFloat(2, 0, 1000),
        'referral_bonus_funds' => $faker->randomFloat(2, 0, 1000)
    ];
});
