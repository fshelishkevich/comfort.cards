<?php

use App\Models\News;
use Faker\Generator as Faker;

$factory->define(News::class, function (Faker $faker) {
    return [
        'title' => $faker->text(30),
        'content' => $faker->text(rand(300, 1000)),
    ];
});
