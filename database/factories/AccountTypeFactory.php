<?php

use App\Models\Account\AccountType;
use Faker\Generator as Faker;

$factory->define(AccountType::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'validity' => $faker->numberBetween(1, 365),
        'price' => $faker->numberBetween(10, 300),
        'min_own_bonus' => $faker->randomFloat(2, 0.5, 1),
        'max_own_bonus' => $faker->randomFloat(2, 1.5, 2),
        'referral_bonus' => $faker->numberBetween(10, 30),
        'max_bonus_amount' => $faker->numberBetween(40, 250)
    ];
});
