<?php

use App\Models\Order\Order;
use Faker\Generator as Faker;

$factory->define(Order::class, function (Faker $faker) {
    return [
        'user_id' => mt_rand(2, 30),
        'account_type_id' => mt_rand(1, 3),
        'order_status_id' => mt_rand(1, 3),
        'random_id' => 'ordr_' . $faker->unique()->text(10)
    ];
});
