<?php

use App\Models\Order\OrderStatus;
use Illuminate\Database\Seeder;

class OrderStatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        OrderStatus::insert([
            ['title' => 'Ожидание оплаты'],
            ['title' => 'Оплачен'],
            ['title' => 'Ошибка оплаты'],
        ]);
    }
}
