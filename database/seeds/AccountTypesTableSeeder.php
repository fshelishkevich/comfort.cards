<?php

use App\Models\Account\AccountType;
use Illuminate\Database\Seeder;

class AccountTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        AccountType::insert([
            [
                'name' => 'Bronze',
                'validity' => 30,
                'price' => 13,
                'min_own_bonus' => 0.7,
                'max_own_bonus' => 1,
                'referral_bonus' => 10,
                'additional_referral_bonus' => null,
                'total_amount_own_bonus' => 15,
            ],
            [
                'name' => 'Silver',
                'validity' => 183,
                'price' => 75,
                'min_own_bonus' => 1.2,
                'max_own_bonus' => 1.5,
                'referral_bonus' => 15,
                'additional_referral_bonus' => 5,
                'total_amount_own_bonus' => 110,
            ],
            [
                'name' => 'Gold',
                'validity' => 365,
                'price' => 150,
                'min_own_bonus' => 1.7,
                'max_own_bonus' => 2,
                'referral_bonus' => 20,
                'additional_referral_bonus' => 10,
                'total_amount_own_bonus' => 300,
            ],
            [
                'name' => 'Базовый',
                'validity' => 30,
                'price' => 1,
                'min_own_bonus' => 0.01,
                'max_own_bonus' => 0.02,
                'referral_bonus' => 0,
                'additional_referral_bonus' => null,
                'total_amount_own_bonus' => 10,
            ],
        ]);
    }
}
