<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(AccountTypesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(TransactionTypesTableSeeder::class);
        $this->call(OrderStatusesTableSeeder::class);
        $this->call(NewsTableSeeder::class);
        $this->call(OrdersTableSeeder::class);
    }
}
