<?php

use App\Models\Account\Account;
use App\Models\Account\AccountType;
use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'login' => 'admin',
            'email' => 'nazarzhuravlev@gmail.com',
            'password' => Hash::make('sdj$*37do)%34;dl'),
            'verified' => true,
            'sponsor_id' => 1,
        ]);

        $users = factory(User::class, 30)->create();

        $card = 15;
        foreach ($users as $u) {
            factory(Account::class)->create([
                'user_id' => $u->id,
                'card_number' => '000000' . $card
            ]);

            $card++;
        }
    }
}
