<?php

use App\Models\Transaction\TransactionType;
use Illuminate\Database\Seeder;

class TransactionTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TransactionType::insert([
            [
                'title' => 'Начисление личных бонусов',
            ],
            [
                'title' => 'Начисление реферальных бонусов',
            ]
        ]);
    }
}
