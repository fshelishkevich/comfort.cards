<?php

namespace App\Http\Controllers\Auth;

use App\Mail\VerifyMail;
use App\User;
use App\Http\Controllers\Controller;
use App\VerifyUser;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Queue\RedisQueue;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Mail;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/account';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware( 'guest' );
    }

    public function showRegistrationForm( Request $request )
    {
        $sponsor = User::whereLogin( $request->get( 'ref' ) )->first();

        if ( $request->get( 'ref' ) ) {
            $this->setSessionWarnings( $sponsor );
        } else {
            session()->forget( 'warning' );
        }

        $sponsorLogin = null;
        if ( ! session()->has( 'warning' ) && $sponsor ) {
            $sponsorLogin = $sponsor->login;
        }

        return view( 'auth.register', ['sponsorLogin' => $sponsorLogin] );
    }

    private function setSessionWarnings( $sponsor )
    {
        if ( $sponsor && ! $sponsor->verified ) {
            session()->flash( 'warning', __( 'warnings.not-verified' ) );
        }

        if ( ! $sponsor ) {
            session()->flash( 'warning', __( 'warnings.referral_link_broken' ) );
        }
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     *
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator( array $data )
    {
        return Validator::make( $data, [
            'login' => [
            	'required', 'string', 'max:255', 'unique:users', 'min:4',
	            'regex:/(^([a-zA-Z]+)(\d+)?$)/u',
	            ],
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'sponsor_login' => [
                'nullable',
                'exists:users,login',
                function ( $attribute, $value, $fail ) {
                    $user = User::whereLogin( $value )->first();
                    if ( $user && ! $user->verified ) {
                        return $fail( __( 'warnings.typed_sponsor_login_not_verified' ) );
                    }
                },
            ]
        ],
            [
                'sponsor_login.exists' => __( 'warnings.user_not_exist' ),
                'login.regex' => __('warnings.login_only_en_string'),
            ] );
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     *
     * @return \App\User
     */
    protected function create( array $data )
    {
        $sponsorId = 1;
        if ( isset( $data['sponsor_login'] ) ) {
            $sponsorId = User::whereLogin( $data['sponsor_login'] )->first()->id;
        }

        $user = User::create( [
            'login' => $data['login'],
            'email' => $data['email'],
            'password' => Hash::make( $data['password'] ),
            'sponsor_id' => $sponsorId,
        ] );

        VerifyUser::create( [
            'user_id' => $user->id,
            'token' => str_random( 40 )
        ] );

        Mail::to( $user->email )->send( new VerifyMail( $user ) );

        return $user;
    }

    public function verifyUser( $token )
    {
        $verifyUser = VerifyUser::whereToken( $token )->withUser()->first();

        if ( isset( $verifyUser ) ) {
            $user = $verifyUser->user;

            if ( ! $user->verified ) {
                $user->verified = true;
                $user->save();
                $status = __( 'statuses.successful_registered' );
            } else {
                $status = __( 'statuses.email_already_confirmed' );
            }
        } else {
            return redirect()->route( 'login' )
                ->with( 'warning', __( 'warnings.email_not_exist' ) );
        }

        return redirect()->route( 'login' )->with( 'status', $status );
    }

    /**
     * @param Request $request
     * @param         $user
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function registered( Request $request, $user )
    {
        $this->guard()->logout();

        return redirect()->route( 'login' )
            ->with( 'warning', __( 'warnings.email_confirmation_sended' ) );
    }
}
