<?php

namespace App\Http\Controllers\Account;

use App\Http\Resources\Account\NewsResource;
use App\Models\News;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class NewsController extends Controller
{
    function index()
    {
        $news = News::paginate(10);

        return NewsResource::collection($news);
    }

    function latestFirst()
    {
        $item = News::latest()->first();

        return new NewsResource($item);
    }
}
