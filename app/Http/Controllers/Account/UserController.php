<?php

namespace App\Http\Controllers\Account;

use App\Http\Resources\Account\UserResource;
use App\User;
use App\Http\Controllers\Controller;
use Auth;

class UserController extends Controller
{
    function getAuthUserWithData()
    {
        $user = User::withAccount()
            ->withAccountType()
            ->withFirstLvlReferralsCount()
            ->findOrFail(Auth::id());

        return new UserResource($user);
    }
}
