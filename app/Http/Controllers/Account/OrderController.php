<?php

namespace App\Http\Controllers\Account;

use App\Http\Requests\Account\OrderRequest;
use App\Models\Order\Order;
use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use function MongoDB\BSON\toJSON;

class OrderController extends Controller
{
    public function store(OrderRequest $r)
    {
        $user = Auth::user();

        $order = new Order();


        $order->user_id = $user->id;
        $order->account_type_id = $r->account_type_id;
        $order->order_status_id = 1; // Ожидание оплаты
        $order->random_id = uniqid('ordr_');
        $order->save();


        return json_encode(['random_id' => $order->random_id]);
    }

    public function success(Request $request)
    {

    }

    public function failed(Request $request)
    {
        $order = $this->setOrderFailed($request);
    }

    private function setOrderFailed(Request $request)
    {
        $order = Order::whereRandomId($request->PAYMENT_ID)->first();
        $order->order_status_id = 3; // Ошибка оплаты
        $order->save();

        return $order;
    }
}
