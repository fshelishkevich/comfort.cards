<?php

namespace App\Http\Controllers\Account;

use App\Http\Controllers\Controller;
use App\Models\Account\AccountType;
use App\Models\News;
use App\User;
use Auth;

class PageController extends Controller
{
    function index()
    {
        $accountTypes = AccountType::whereKeyNot(4)->get();

        $news = News::latest()->first();

        return view( 'account.app', compact(['user', 'news', 'accountTypes']));
    }
}
