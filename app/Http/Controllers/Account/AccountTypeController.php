<?php

namespace App\Http\Controllers\Account;

use App\Http\Resources\Account\AccountTypeResource;
use App\Models\Account\AccountType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AccountTypeController extends Controller
{
    function getAllExceptBasic()
    {
        $accounts = AccountType::whereKeyNot(4)->get();

        return AccountTypeResource::collection($accounts);
    }
}
