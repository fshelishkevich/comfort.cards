<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\NewsRequest;
use App\Http\Resources\Admin\NewsResource;
use App\Models\News;

class NewsController extends Controller
{
    function index()
    {
        $news = News::paginate(10);

        return NewsResource::collection($news);
    }


    public function store(NewsRequest $request)
    {
        News::create([
            'title' => $request->title,
            'content' => $request->get('content'),
        ]);
    }


    public function update(NewsRequest $request, $id)
    {
        News::findOrFail($id)->update([
            'title' => $request->title,
            'content' => $request->get('content'),
        ]);
    }
}
