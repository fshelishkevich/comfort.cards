<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\AccountTypesRequest;
use App\Http\Resources\Admin\AccountTypes;
use App\Models\Account\AccountType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AccountTypeController extends Controller
{
    public function index()
    {
        $accountTypes = AccountType::paginate(10);

        return AccountTypes::collection($accountTypes);
    }


    public function store(AccountTypesRequest $request)
    {
        echo 'test';
    }


    public function update(Request $request, $id)
    {
        //
    }


    public function destroy($id)
    {
        //
    }
}
