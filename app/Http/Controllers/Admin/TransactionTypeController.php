<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\TransactionTypeRequest;
use App\Http\Resources\Admin\TransactionTypeResource;
use App\Models\Transaction\TransactionType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TransactionTypeController extends Controller
{
    function index()
    {
        $transactionTypes = TransactionType::paginate(10);

        return TransactionTypeResource::collection($transactionTypes);
    }

    function store(TransactionTypeRequest $r)
    {
        $t = new TransactionType();
        $t->title = $r->title;
        $t->save();
    }

    function update(TransactionTypeRequest $r, $id)
    {
        $t = TransactionType::findOrFail($id);
        $t->title = $r->title;
        $t->save();
    }
}
