<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\UsersRequest;
use App\Http\Resources\Admin\Users;
use App\Models\Account\Account;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    public function index()
    {
        $users = User::withFirstLvlReferralsCount()
            ->withAccountType()
            ->paginate(10);

        return Users::collection($users);
    }


    public function store(UsersRequest $request)
    {

    }

    public function update(UsersRequest $request, $id)
    {
        $user = User::findOrFail($id);

        if(! $user->account){
            $this->createAccount($user->id);
        }

        $user->account()->update([
            'accounts.card_number' => $request->card_number,
            'accounts.account_type_id' => $request->account_type_id ?? 4,
            'accounts.has_been_paid_at' => Carbon::now(),
        ]);
    }


    public function destroy($id)
    {

    }

    private function createAccount($userId)
    {
        Account::unguard();

        Account::create([
            'user_id' => $userId,
            'account_type_id' => 4,
            'card_number' => 'NA',
            'has_been_paid_at' => Carbon::now(),
            'expiry_date' => Carbon::now()->addDays(30),
            'is_active' => 1,
        ]);
    }
}
