<?php

namespace App\Http\Controllers\Admin;

use App\Http\Resources\Admin\OrderResource;
use App\Models\Order\Order;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OrderController extends Controller
{
    function index()
    {
        $orders = Order::with(['orderStatus', 'account', 'orderStatus', 'accountType'])
            ->withUser()
            ->paginate(10);

        return OrderResource::collection($orders);
    }
}
