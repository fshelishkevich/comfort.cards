<?php

namespace App\Http\Requests\Admin;

use App\User;
use Illuminate\Foundation\Http\FormRequest;

class UsersRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $editedUser = User::withAccount()
            ->findOrFail($this->route()->parameters['user']);

        if($this->isMethod('put')){
            // if account exists
            if($editedUser->account){
                $accountId = $editedUser->account->id;

                return [
                    'card_number' => 'required|digits:8|unique:accounts,card_number,' . $accountId,
                ];
            }

            return [
                'card_number' => 'required|digits:8|unique:accounts,card_number,',
            ];
        }

        return [
            'login' => [
                'required', 'string', 'max:255', 'unique:users', 'min:4',
                'regex:/(^([a-zA-Z]+)(\d+)?$)/u',
            ],
            'email' => 'required|string|email|max:255|unique:users',
        ];
    }

    public function messages()
    {
        return [
            'card_number.digits' => 'Поле должно состоять из 8 цифр',
        ];
    }
}
