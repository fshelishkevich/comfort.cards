<?php

namespace App\Http\Requests\Account;

use App\Models\Account\AccountType;
use Illuminate\Foundation\Http\FormRequest;

class OrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $accountTypes = AccountType::count();

        return [
            'account_type_id' => 'required|numeric|between:1,' . $accountTypes
        ];
    }
}
