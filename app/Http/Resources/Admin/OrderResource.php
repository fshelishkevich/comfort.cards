<?php

namespace App\Http\Resources\Admin;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $createdAt = new Carbon($this->created_at);

        return [
            'id' => $this->id,
            'login' => $this->account->user->login,
            'email' => $this->account->user->email,
            'account_type' => $this->accountType->name,
            'status' => $this->orderStatus->title,
            'created_at' => $createdAt->toDateString(),
        ];
    }
}
