<?php

namespace App\Http\Resources\Admin;

use Illuminate\Http\Resources\Json\JsonResource;

class AccountTypes extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'validity' => $this->validity,
            'price' => $this->price,
            'min_own_bonus' => $this->min_own_bonus,
            'max_own_bonus' => $this->max_own_bonus,
            'referral_bonus' => $this->referral_bonus,
            'additional_referral_bonus' => $this->additional_referral_bonus,
            'total_amount_own_bonus' => $this->total_amount_own_bonus,
        ];
    }
}
