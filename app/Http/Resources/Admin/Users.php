<?php

namespace App\Http\Resources\Admin;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class Users extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $registerDate = new Carbon($this->created_at);

        return [
            'id' => $this->id,
            'login' => $this->login,
            'email' => $this->email,
            'verified' => $this->verified,
            'created_at' => $registerDate->toDateString(),
            'first_lvl_referrals_count' => $this->first_lvl_referrals_count,
            'own_bonus_funds' => $this->account ? $this->account->own_bonus_funds : '',
            'own_bonus_awarding_is_active' => $this->account ? $this->account->is_active : false,
            'card_number' => $this->account ? $this->account->card_number : '',
            'account_type' => $this->account ? $this->account->accountType->name : '',
            'account_type_id' => $this->account ? $this->account->account_type_id : '',
            'has_been_paid_at' => $this->account ? $this->account->has_been_paid_at : '',
        ];
    }
}
