<?php

namespace App;

use App\Models\Account\Account;
use App\Notifications\MailResetPasswordToken;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * App\User
 *
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @mixin \Eloquent
 * @property int $id
 * @property string $login
 * @property string $email
 * @property string $password
 * @property int $verified
 * @property string|null $remember_token
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\VerifyUser $verifyUser
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereLogin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereVerified($value)
 * @property int $sponsor_id
 * @property string $referral_token
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereReferralToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereSponsorId($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\User[] $firstLvlReferrals
 * @property-read \App\User $sponsor
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User withFirstLvlReferrals()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User withFirstLvlReferralsCount()
 * @property int $is_banned
 * @property-read \App\Models\Account\Account $account
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereIsBanned($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User withAccount()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User withAccountType()
 */
class User extends Authenticatable
{
    use Notifiable;

    public function sendPasswordResetNotification( $token )
    {
        $this->notify( new MailResetPasswordToken( $token ) );
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'login', 'email', 'password', 'verified', 'sponsor_id', 'referral_token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function isAdmin()
    {
        return $this->id == 1 ? true : false;
    }

    /*
     * ---------------------------------------------
     *		Relationships
     * ---------------------------------------------
     */
    public function account()
    {
        return $this->hasOne(Account::class);
    }

    public function scopeWithAccount($query)
    {
        return $query->with('account');
    }

    public function scopeWithAccountType($query)
    {
        return $query->with('account.accountType');
    }


    public function firstLvlReferrals()
    {
        return $this->hasMany(User::class, 'sponsor_id');
    }

    public function scopeWithFirstLvlReferrals($query)
    {
        return $query->with('firstLvlReferrals');
    }

    public function scopeWithFirstLvlReferralsCount($query)
    {
        return $query->withCount('firstLvlReferrals');
    }



    public function verifyUser()
    {
        return $this->hasOne(VerifyUser::class);
    }

    public function sponsor()
    {
        return $this->belongsTo(User::class, 'sponsor_id');
    }
}
