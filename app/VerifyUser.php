<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\VerifyUser
 *
 * @mixin \Eloquent
 * @property int $id
 * @property int $user_id
 * @property string $token
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VerifyUser whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VerifyUser whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VerifyUser whereToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VerifyUser whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VerifyUser whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VerifyUser withUser()
 */
class VerifyUser extends Model
{
    protected $guarded = [];

    /*
     * ---------------------------------------------
     *		Relationships
     * ---------------------------------------------
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /*
     * ---------------------------------------------
     *		Scopes
     * ---------------------------------------------
     */
    public function scopeWithUser($query)
    {
        /** @var VerifyUser $query */
        return $query->with('user');
    }
}
