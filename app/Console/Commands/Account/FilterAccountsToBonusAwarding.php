<?php

namespace App\Console\Commands\Account;

use App\ComfortCards\BonusFilter;
use Illuminate\Console\Command;

class FilterAccountsToBonusAwarding extends Command
{
    protected $signature = 'account:filter-accounts-to-bonus-awarding';


    protected $description = 'This command sets is active to false for accounts those was expired or those was already get their own max of bonuses';


    public function __construct()
    {
        parent::__construct();
    }


    public function handle(BonusFilter $expiredAccount)
    {
        $expiredAccount->switchOffBonusAwardingToAccountsWithExpiredTerms();
    }
}
