<?php

namespace App\Console\Commands\Account;

use App\ComfortCards\AccountBonus;
use Illuminate\Console\Command;

class AwardOwnBonuses extends Command
{
    protected $signature = 'account:award-own-bonuses';

    protected $description = 'Command that accruals bonuses';


    public function __construct()
    {
        parent::__construct();
    }


    public function handle(AccountBonus $ab)
    {
        $ab->awardOwnBonuses();
    }
}
