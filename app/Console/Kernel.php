<?php

namespace App\Console;

use App\Console\Commands\Account\AwardOwnBonuses;
use App\Console\Commands\Account\FilterAccountsToBonusAwarding;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        AwardOwnBonuses::class,
        FilterAccountsToBonusAwarding::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('account:filter-accounts-to-bonus-awarding')
            ->weekdays()
            ->at('1:00');

        $schedule->command('account:award-own-bonuses')
            ->weekdays()
            ->at('2:00');
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
