<?php

namespace App\Models\Transaction;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Transaction\TransactionType
 *
 * @mixin \Eloquent
 * @property int $id
 * @property string $title
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Transaction\TransactionType whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Transaction\TransactionType whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Transaction\TransactionType whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Transaction\TransactionType whereUpdatedAt($value)
 */
class TransactionType extends Model
{
    public $timestamps = false;
}
