<?php

namespace App\Models\Transaction;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Transaction\Transaction
 *
 * @mixin \Eloquent
 * @property int $id
 * @property int $user_id
 * @property int $account_id
 * @property int $transaction_type_id
 * @property float $amount Total money sum of the transaction
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Transaction\Transaction whereAccountId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Transaction\Transaction whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Transaction\Transaction whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Transaction\Transaction whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Transaction\Transaction whereTransactionTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Transaction\Transaction whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Transaction\Transaction whereUserId($value)
 */
class Transaction extends Model
{
    //
}
