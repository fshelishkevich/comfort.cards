<?php

namespace App\Models\Account;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Account\AccountType
 *
 * @property int $id
 * @property string $name
 * @property int $validity Amount of a days after which the account will be disabled
 * @property int $price
 * @property float $min_own_bonus
 * @property float $max_own_bonus
 * @property int $referral_bonus
 * @property int $max_bonus_amount Total money that can be received on this type of account as a bonus
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Account\AccountType whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Account\AccountType whereMaxBonusAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Account\AccountType whereMaxOwnBonus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Account\AccountType whereMinOwnBonus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Account\AccountType whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Account\AccountType wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Account\AccountType whereReferralBonus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Account\AccountType whereValidity($value)
 * @mixin \Eloquent
 * @property int|null $additional_referral_bonus Bonus for the referrals from 2d till 10th level
 * @property int $total_amount_own_bonus Total money that can be received on this type of account as the own bonus
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Account\AccountType whereAdditionalReferralBonus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Account\AccountType whereTotalAmountOwnBonus($value)
 * @property-read mixed $own_bonus_percentage
 * @property-read mixed $u_a_h_price
 */
class AccountType extends Model
{
    public $timestamps = false;

    /*
     * ---------------------------------------
     * Accessors
     * ---------------------------------------
    */

    /**
     * We need to get a random percentage of the current account
     * type percentage range to pay bonuses
     */
    public function getOwnBonusPercentageAttribute()
    {
        $percentage = mt_rand($this->min_own_bonus *1000, $this->max_own_bonus * 1000) / 1000;

        return $percentage;
    }

    public function getUAHPriceAttribute()
    {
        return $this->price * 27.7;
    }
}
