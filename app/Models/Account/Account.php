<?php

namespace App\Models\Account;

use App\User;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Account\Account
 *
 * @property int $id
 * @property int $user_id
 * @property int|null $account_type_id
 * @property string|null $card_number
 * @property string|null $has_been_paid_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Account\Account whereAccountTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Account\Account whereCardNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Account\Account whereHasBeenPaidAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Account\Account whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Account\Account whereUserId($value)
 * @mixin \Eloquent
 * @property-read \App\Models\Account\AccountType|null $accountType
 * @property string $expiry_date
 * @property int $is_active
 * @property float $own_funds
 * @property float $own_bonus_funds
 * @property float $referral_bonus_funds
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Account\Account whereExpiryDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Account\Account whereIsActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Account\Account whereOwnBonusFunds($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Account\Account whereOwnFunds($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Account\Account whereReferralBonusFunds($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Account\Account withAccountType()
 * @property-read \App\User $user
 */
class Account extends Model
{
    public $timestamps = false;

    /*
     * ---------------------------------------------
     *		Relationships
     * ---------------------------------------------
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

     public function accountType()
     {
         return $this->belongsTo(AccountType::class);
     }

     public function scopeWithAccountType($query)
     {
         return $query->with('accountType');
     }
}
