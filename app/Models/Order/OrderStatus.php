<?php

namespace App\Models\Order;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Order\OrderStatus
 *
 * @property int $id
 * @property string $title
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order\OrderStatus whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order\OrderStatus whereTitle($value)
 * @mixin \Eloquent
 */
class OrderStatus extends Model
{
    public $timestamps = false;
}
