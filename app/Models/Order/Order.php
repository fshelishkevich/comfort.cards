<?php

namespace App\Models\Order;

use App\Models\Account\Account;
use App\Models\Account\AccountType;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Order\Order
 *
 * @property int $id
 * @property int $account_id
 * @property int $account_type_id
 * @property int $order_status_id
 * @property int $payment_success Is the payment was success
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order\Order whereAccountId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order\Order whereAccountTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order\Order whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order\Order whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order\Order whereOrderStatusId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order\Order wherePaymentSuccess($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order\Order whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int $user_id
 * @property string $random_id Unique random id that will be used in payment APIs
 * @property-read \App\Models\Account\Account $account
 * @property-read \App\Models\Account\AccountType $accountType
 * @property-read \App\Models\Order\OrderStatus $orderStatus
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order\Order whereRandomId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order\Order whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order\Order withUser()
 */
class Order extends Model
{
    function account()
    {
        return $this->belongsTo(Account::class);
    }

    function accountType()
    {
        return $this->belongsTo(AccountType::class);
    }

    function orderStatus()
    {
        return $this->belongsTo(OrderStatus::class);
    }

    function scopeWithUser($query)
    {
        return $query->with('account.user');
    }
}
