<?php


namespace App\ComfortCards;


use App\Models\Account\Account;

class AccountBonus
{
    private $accounts;
    private $percentage;

    public function __construct()
    {
        $this->accounts = Account::whereIsActive(true)
            ->withAccountType()
            ->get();
    }

    public function awardOwnBonuses()
    {
        foreach ($this->accounts as $a) {
            // This variable will be used also in transaction history in future
            // We gets it through accessor, so it gets different values on each request
            $this->percentage = $a->accountType->own_bonus_percentage;

            $bonus = $a->accountType->uah_price * $this->percentage / 100;

            $a->own_bonus_funds += $bonus;
            $a->save();
        }
    }
}