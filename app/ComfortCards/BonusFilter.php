<?php


namespace App\ComfortCards;


use App\Models\Account\Account;
use Carbon\Carbon;

class BonusFilter
{
    private $accounts;

    public function __construct()
    {
        $this->accounts = Account::whereIsActive(true)
            ->withAccountType()
            ->get();
    }

    public function switchOffBonusAwardingToAccountsWithExpiredTerms()
    {
        /** @var Account $a */
        foreach ($this->accounts as $a) {
            $paidDate = new Carbon($a->has_been_paid_at);
            $expireDate = $paidDate->addDays($a->accountType->validity);
            $a->expiry_date = $expireDate;

            if($expireDate->timestamp < Carbon::now()->timestamp){
                $a->is_active = false;
            }

            if($a->own_bonus_funds >= $a->accountType->total_amount_own_bonus * 28){
                $a->is_active = false;
            }

            $a->save();
        }
    }
}