<?php

$devMiddleware = (app()->environment() == 'dev') ? 'admin' : 'web';

Route::group(['middleware' => $devMiddleware], function(){
    Route::get('/', 'PageController@index')->name('pages.index');

    Route::get('/user/verify/{token}', 'Auth\RegisterController@verifyUser')->name('user.verify');

    Route::get('/home', function(){
        return redirect()->route('pages.index');
    })->name('home');
});

Auth::routes();