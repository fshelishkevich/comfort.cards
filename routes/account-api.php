<?php

Route::group(['prefix' => 'account-api', 'as' => 'account-api', 'namespace' => 'Account'], function(){
    Route::group(['prefix' => 'news'], function(){
        Route::get('/', 'NewsController@index');
        Route::get('latest-first', 'NewsController@latestFirst');
    });

    Route::get('/account-types', 'AccountTypeController@getAllExceptBasic');

    Route::get('/user-data', 'UserController@getAuthUserWithData');


    Route::group(['prefix' => 'order'], function(){
        Route::post('store', 'OrderController@store');
        Route::post('success', 'OrderController@success');
        Route::post('failed', 'OrderController@failed');
    });
});