<?php

Route::group(['namespace' => 'Admin'], function(){
    // If you need to add an additional route to the resource, please do so above the corresponding "Route :: resource"

    Route::resource('users', 'UserController')->except(['create', 'edit']);
    Route::resource('account-types', 'AccountTypeController')->except(['create', 'edit']);
    Route::resource('news', 'NewsController')->except(['create', 'edit']);
    Route::resource('transaction-types', 'TransactionTypeController')->except(['create', 'edit']);



    Route::resource('orders', 'OrderController')->except(['create', 'edit', 'destroy']);
});
