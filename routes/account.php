<?php

$devMiddleware = (app()->environment() == 'dev') ? 'admin' : 'web';

Route::group(['middleware' => $devMiddleware, 'prefix' => 'account', 'as' => 'account.', 'namespace' => 'Account'], function(){

    Route::get( '/', 'PageController@index' )
        ->name( 'index' );
});