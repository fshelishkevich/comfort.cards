let mix = require('laravel-mix')
/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

/*
 * ---------------------------------------------
 *		Code that optimizes images
 * ---------------------------------------------
 */
// let ImageMinPlugin = require('imagemin-webpack-plugin').default;
// let CopyWebpackPlugin = require('copy-webpack-plugin');
// let ImageMinMozjpeg = require('imagemin-mozjpeg');
// mix.webpackConfig( {
//     plugins: [
//         new CopyWebpackPlugin([{
//             from: 'resources/assets/images',
//             to: 'images', // Laravel mix will place this in 'public/images'
//         }]),
//         new ImageMinPlugin( {
// //            disable: process.env.NODE_ENV !== 'production', // Disable during development
//             test: /\.(jpe?g|png|gif|svg)$/i,
//             plugins: [
//                 ImageMinMozjpeg({
//                     quality: 60,
//                 })
//             ]
//         } ),
//     ],
// } )
//
mix
    .js('resources/assets/js/site.js', 'public/js')
    .js('resources/assets/js/admin.js', 'public/js')
    .js('resources/assets/js/account.js', 'public/js')

    .sass('resources/assets/sass/site.scss', 'public/css')
    .sass('resources/assets/sass/admin.scss', 'public/css')
    .sass('resources/assets/sass/account.scss', 'public/css')

    // .copy('resources/assets/images', 'public/images')